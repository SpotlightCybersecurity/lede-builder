**UPDATE**: LEDE has merged back to OpenWRT *and* I'm also no longer regularly building OpenWRT images or packages (trying to use the stock image as much as possible). This code is no longer maintained but is left as an example for others who might want to try the same thing.

I found it difficult to build LEDE in my Linux desktop (which is not Debian/Ubuntu). Building in a Docker container based on Debian/Ubuntu has proved to be much more reliable. A [few][2] [others][3] have done similar things. I wanted a more generic environment that I could control the build in, but this work is heavily influenced by them. Thanks for sharing!

# Prepare the container environment
1. Make a directory to do the build in: ```mkdir ~/lede```
1. The default container runs as user id 1000 (a user named builder). chown/chmod, as appropriate, the directory so that user can write to it.
1. Run the container ```sudo docker run --rm -v ~/lede:/src -it spotlightcybersecurity/lede-builder``` (which will drop you to a bash shell)

# In the container
Once in the container, you can follow [LEDE/OpenWRT's build instructions][1]. But here's quick summary:
## First time (download code)
1. Download the source code (Replace the '-b lede-17.01' with a different version if you don't want the 17.01 series of releases): ```git clone -b lede-17.01 https://github.com/lede-project/source.git```
1. Update the feeds (external packages that get imported and built): ```./scripts/feeds update -a```
1. Pick which feeds you want ```./scripts/feeds install PACKAGENAMEHERE``` or install them all ```./scripts/feeds install -a``` (install here means to pull their code into the buildroot so you can then choose to build them from the menuconfig).

## Each time
1. Configure from scratch:
    1. ```make menuconfig``` to select target
    1. ```make defconfig``` to put all the defaults in place for that target
    1. ```make menuconfig``` to customize all your options
1. Or, if you have an existing config, configure from an existing config file:
    1. ```cp CONFIGNAME .config```
    1. ```make defconfig``` to take the specified config and fill in any defaults and prerequisites
    1. ```make menuconfig``` to customize
1. If you have any custom files you want to include in the built image (e.g. default configurations), ```mkdir files``` and then ```cp FILETOINCLUDE files/```. For example, I usually include OpenDNS' resolv.conf file ```mkdir files/etc; cp resolv.conf.opendns files/etc/resolv.conf.opendns```
1. Now build: ```make``` for a quiet build or ```make V=s``` for a verbose build (easier to see errors)
1. Look in 'bin' for the build images!

# Troubleshooting
Building takes a long time and uses a lot of space. If "weird" errors occur during the build, first check disk space. Running out of space during a build can cause some errors that sound unrelated to disk space, but are in fact caused because some build component couldn't make the file it needed for the next step.

If errors occur while building a particular package, clean that particular package by typing ```make package/PACKAGEPATH/clean``` for example: ```make package/network/utils/linux-atm/clean```

# See Also
* [Docker container which builds a very particular configuration](https://github.com/Acris/docker-lede-netgear-r6100)

[1]: https://lede-project.org/docs/guide-developer/use-buildsystem
[2]: https://hub.docker.com/r/acrisliu/lede/
[3]: https://noah.meyerhans.us/blog/2015/03/19/building-openwrt-with-docker/
