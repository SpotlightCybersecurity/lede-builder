# inspired by https://noah.meyerhans.us/blog/2015/03/19/building-openwrt-with-docker/
# and https://hub.docker.com/r/acrisliu/lede/
FROM debian:stretch-slim

MAINTAINER ryan@spotlightcybersecurity.com

# pulled the package list from:
# https://wiki.openwrt.org/doc/howto/buildroot.exigence

# use libssl1.0-dev because of some uboot code:
# https://github.com/freifunk-gluon/gluon/issues/973
RUN apt-get update && apt-get install -y build-essential libncurses5-dev gawk git subversion gettext unzip zlib1g-dev file python wget openssl libssl1.0-dev && apt-get clean

# Build as a user - change the user ID if needed to match your desired
# user ID
RUN mkdir /src && useradd -u 1000 builder
USER builder

# Mount the code and the build environment at /src (it's a volume so the build
# files and the toolchain can persist from container to container)
VOLUME /src
WORKDIR /src

CMD ["/bin/bash"]
